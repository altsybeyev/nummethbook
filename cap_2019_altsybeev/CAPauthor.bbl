\begin{thebibliography}{}

\bibitem[MAD, 2011]{MAD1}
 (2011).
\newblock {MAD --- Methodical Accelerator Design. CERN-BE/ABP Accelerator Beam
  Physics Group}.
\newblock http://madx.web.cern.ch/madx/.
\newblock Accessed: 2019-09-06.

\bibitem[Altsybeyev, 2017]{DAISI}
Altsybeyev, V. (2017).
\newblock Configurable code for beam dynamics simulations in electrostatic
  approximation.
\newblock In {\em 2016 Young Researchers in Vacuum Micro/Nano Electronics,
  VMNE-YR 2016 - Proceedings}, number 7880398.

\bibitem[Altsybeyev et~al., 2018a]{RFQ}
Altsybeyev, V., Svistunov, Y., Durkin, A., and Ovsyannikov, D. (2018a).
\newblock {Preacceleration of the multicharged ions with the different A/Z
  ratios in single radio-frequency quadrupole(RFQ) channel}.
\newblock {\em Cybernetics and Physics}, {\bfseries 7}, pp.~49--56.

\bibitem[Altsybeyev et~al., 2018b]{nica1}
Altsybeyev, V.~V., Butenko, A.~V., Emelianenko, V., and et~al. (2018b).
\newblock {Simulation of Closed Orbit Correction for the Nuclotron Booster}.
\newblock {\em Physics of Particles and Nuclei Letters}, {\bfseries 15},
  pp.~854--857.

\bibitem[Altsybeyev et~al., 2018c]{nica2}
Altsybeyev, V.~V., Kozlov, O., Kozynchenko, V., and et~al. (2018c).
\newblock {Development of Software for Simulating and Analyzing the Dynamics of
  Charged-Particle Beams in Synchrotrons and Beam Lines}.
\newblock {\em Physics of Particles and Nuclei Letters}, {\bfseries 15},
  pp.~798--801.

\bibitem[Ovsyannikov et~al., 2014]{altsybeyev2}
Ovsyannikov, A.~D., Ovsyannikov, D.~A., Altsybeyev, V.~V., Durkin, A.~P., and
  Papkovich, V.~G. (2014).
\newblock Application of optimization techniques for {RFQ} design.
\newblock {\em Problems of Atomic Science and Technology}, {\bfseries 3}\,(91),
  pp.~116--119.

\end{thebibliography}
