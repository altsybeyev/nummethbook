%%%%%%%%%%%%%%%%%%%% author.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sample root file for your "contribution" to a contributed volume
%
% Use this file as a template for your own input.
%
%%%%%%%%%%%%%%%% Springer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% RECOMMENDED %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[graybox]{svmult}
\bibliographystyle{spmpsci}

% choose options for [] as required from the list
% in the Reference Guide

\usepackage{mathptmx}       % selects Times Roman as basic font
\usepackage{helvet}         % selects Helvetica as sans-serif font
\usepackage{courier}        % selects Courier as typewriter font
\usepackage{type1cm}        % activate if the above 3 fonts are
                            % not available on your system
%
\usepackage{makeidx}         % allows index generation
\usepackage{graphicx}        % standard LaTeX graphics tool
                             % when including figure files
\usepackage{multicol}        % used for the two-column index
\usepackage[bottom]{footmisc}% places footnotes at page bottom
\usepackage{amsmath}
% see the list of further useful packages
% in the Reference Guide

\makeindex             % used for the subject index
                       % please use the style svind.ist with
                       % your makeindex program

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title*{An optimization approach for minimization of charged particles orbit deviation in synchrotron and transport channels systems caused by magnetic field tolerances}
% Use \titlerunning{Short Title} for an abbreviated version of
% your contribution title if the original one is too long
\author{Vladislav Altsybeyev and Vladimir  Kozynchenko}
% Use \authorrunning{Short Title} for an abbreviated version of
% your contribution title if the original one is too long
\institute{Vladislav Altsybeyev \at Saint Petersburg University, Russia, \email{v.altsybeev@spbu.ru}
		\and Vladimir  Kozynchenko \at Saint Petersburg University, Russia \email{v.kozynchenko@spbu.ru}
}

\maketitle

\abstract{Optimization methods of synchrotrons and transport channels systems are considered in this report. The significant deviation of orbit of charged particles beam in this facilities causes by magnetic field tolerances. This tolerances may appeared because of deviation of real magnet length on theoretically calculated. In this report we discuss the approach of the fast channel parameter optimization technique by using swarm computations and gradient descend for improving the beam orbit. The optimizations aimed at choosing angles of correctors elements in structure of acceleration system. }

\section{Introduction}
\label{sec:1}
In the case of presence the relative magnetic field tolerances deviations of trajectories of charged particles may significant enlarge and this may cause enlarging beam losses. The typical solution of this problem is using such orbit correction algorithms based on singular value decomposition of response matrix. The purpose of this report is to study the new approach based on possibility to use swarm computations and gradient descend methods to minimize effect of enlarging deviations of charged particles trajectories. For example at present the main purposes of the discussed approach is solving the problems related with booster synchrotron of MegaScience NICA Accelerator Complex.  


\section{Orbit dynamics simulations and optimization}
\label{sec:2}
\subsection{Equations and problem statement}
In this work we use the Design of Accelerators, optImizations and SImulations (DAISI) code for all calculations \cite{daisi, DAISIpr}. 

For dynamics modeling of charged particles in synchrotrons and transport channels we use linear approximation of magnetic fields.

 \begin{equation*}
 X_{i+1}=
  MX_{i}+b. \quad X=\begin{pmatrix}
 x  \\
 x^{'}  \\
 y    \\
 y^{'}. 
 \end{pmatrix}
 \end{equation*}

Here $x$, $x^{'}$, $y$, $y^{'}$ are positions and velocities of charged particles; $i$ is number of magnet element; $M$ and $b$ are transport matrix and vector respectively describes influence of corresponding $i^{th}$ magnetic element on the particle trajectory. Below in this report we will use the following magnet elements with corresponded $M$ and $b$.

\begin{enumerate}
	\item Drift (empty element):
	 \begin{equation*}
	M = \begin{pmatrix}
	1 & L & 0 & 0 \\
	0 & 1 & 0 & 0  \\
	0 & 0 & 1 & L    \\
	0 & 0 & 0 & 1 
	\end{pmatrix}, \quad
	b = \begin{pmatrix}
	0  \\
	0  \\
	0    \\
	0  
	\end{pmatrix}.	
	\end{equation*}
	Here $L$ is corrector length. 	

	\item Corrector:
	\begin{equation*}
	M = \begin{pmatrix}
	1 & L & 0 & 0 \\
	0 & 1 & 0 & 0  \\
	0 & 0 & 1 & L    \\
	0 & 0 & 0 & 1 
	\end{pmatrix}, \quad
	b = \begin{pmatrix}
	0.5L\varphi\\
	\varphi  \\
	0.5L\psi \\
	\psi    \\
	\end{pmatrix}.	
	\end{equation*}
	Here $\varphi$ and $\psi$ are corrector angles in horizontal and vertical planes respectively; $L$ is corrector length. 	

	\item Quadrupole:
	\begin{equation*}
		b = \begin{pmatrix}
	0  \\
	0  \\
	0    \\
	0  
	\end{pmatrix},\quad
	M = \begin{pmatrix}
	\cos(L\sqrt{K}) & \frac{\sin(L\sqrt{K})}{\sqrt{K}} & 0 & 0 \\
	-\sqrt{K}\sin(L\sqrt{K}) & \cos(L\sqrt{K}) & 0 & 0  \\
	0 & 0 & \cosh(L\sqrt{K}) & \frac{\sinh(L\sqrt{K})}{\sqrt{K}}    \\
	0 & 0 & \sqrt{K}\sinh(L\sqrt{K}) & \cosh(L\sqrt{K}) 
	\end{pmatrix} 
	\end{equation*}	
	for $K>0$ and
	\begin{equation*}
		b = \begin{pmatrix}
	0  \\
	0  \\
	0    \\
	0  
	\end{pmatrix},\quad
	M = \begin{pmatrix}
	\cosh(L\sqrt{K}) & \frac{\sinh(L\sqrt{K})}{\sqrt{K}} & 0 & 0 \\
	\sqrt{K}\sinh(L\sqrt{K}) & \cosh(L\sqrt{K}) & 0 & 0  \\
	0 & 0 & \cos(L\sqrt{K}) & \frac{\sin(L\sqrt{K})}{\sqrt{K}}    \\
	0 & 0 & -\sqrt{K}sin(L\sqrt{K}) & \cos(L\sqrt{K}) 
	\end{pmatrix} 
	\end{equation*}
	for $K<0$ where  $K$ is quadrupole coefficient.	
	
	\item Sector magnet:
	\begin{equation*}
	M = \begin{pmatrix}
	\cos(\alpha)      & R_0\sin(\alpha) & 0 & 0 \\
	-\sin(\alpha)/R_0 & \cos(\alpha) & 0 & 0  \\
	0 & 0 & 1 & \alpha R_0    \\
	0 & 0 & 0 & 1 
	\end{pmatrix}, \quad
	b = \begin{pmatrix}
	R_0\sigma(1-\cos(\alpha))  \\
	\sigma\sin(\alpha)  \\
	0    \\
	0  
	\end{pmatrix}. 
	\end{equation*}	
	Here $R_0$ and $\alpha$ is rotation radius and angle in horizontal plane, $\sigma$ is relative magnetic field tolerance.
	
\end{enumerate}

So in this system $\varphi$ and $\psi$ (corrector angles in horizontal and vertical planes respectively) are control parameters we can chose their to change beam dynamics.

Let us call trajectory $X_i$ obtained using this equations with zero initial conditions $X_0=0$ as orbit.

If the values of relative magnetic field error is zero, the deviation of orbit may be zero ($X_i=0$). In the case of presence the relative magnetic field tolerances ($\sigma$) the $X_i$ may significant enlarge and this may cause enlarging beam losses.     

To solve the optimization problem we need to obtain the two sets of channel parameters $\overline{\varphi}=(\varphi_1, \ldots, \varphi_M)$, $\overline{\psi}=(\psi_1, \ldots, \psi_M)$ where $M$ is total number of corrector element and we can formalize problem as minimization the following function:

\begin{equation*}
F(\overline{\varphi}, \overline{\psi})=\sum_{i=0}^{i=N}x_i^2+y_j^2. \rightarrow min
\end{equation*}	

Here $N$ is total number of optic elements.

\subsection{Optimization approach}
We use approach based on sequential application of particle swarm evolutionary optimization algorithm \cite{Kennedy} and gradient descent optimization. 

At the first stage we use particle swarm method, it does not guarantee achievement of the local fitness function minimum, but it can provide a good initial approximation of the solution. 

After the particle swarm method stage work is done, it is possible to significant improve the obtained solution using application the gradient descend method with initial conditions obtained from particle swarm. We use finite-differences for calculation of gradient. So we dont needed any analytical calculations. Alse the useful "heavy ball" modification of the gradient descent method uses to improve convergence:

\begin{align*}
	{\bf x}_i^n={\bf x}_i^{n-1}+\sigma^k\frac{G({\bf x}^{n-1} + \Delta {\bf x}_i^{n-1})-G({\bf x}_i^{n-1})}{\Delta {\bf x}_i^{n-1}}+\beta({\bf x}^{n-1} - {\bf x}^{n-2}).
\end{align*}
Here ${x}_i^n$ is the $i^{th}$ component of solution ${\bf x}^{n-1}$ on $n^{th}$ algorithm iteration; $\sigma^n$ is the gradient step on the $k^{th}$ iteration; $\Delta {\bf x}_i^{k-1}$ is the small argument deviation for finite difference; $\beta$ is the predefined "weight" parameter; $G$ is the minimized function.

\section{Example of orbit deviation minimization in Booster synchrotron}
In Fig. \ref{fig1} presented trajectory of beam orbit in the Booster synchrotron of NICA Accelerator Complex with taking into account tolerances of magnetic field cases by deviation of real magnet length on theoretically calculated \cite{nica1, nica2}. The presence of this tolerances results in significant enlargement of orbit amplitude oscillation. We provide a set of calculations using  particle swarm(Fig. \ref{fig2}) and combination of particle swarm and gradient descent methods with $\beta=0$ (Fig. \ref{fig3}, \ref{fig4}) to choose angles of correctors elements (its initial values are zero). As you can see from provided figures, oscillation amplitudes of beam orbit trajectories were significantly reduced of by using the above described optimization procedure.

\begin{figure}[t]
	\includegraphics[scale=0.12]{xz-and-yz.pdf}
	\caption{Trajectories of beam orbit before optimization}
	\label{fig1}      
\end{figure}

\begin{figure}[t]
	\includegraphics[scale=0.12]{afteropt_sw.pdf}
	\caption{Trajectories of beam orbit before and after optimization using particle swarm methods}
	\label{fig2}      
\end{figure}

\begin{figure}[t]
	\includegraphics[scale=0.12]{afteropt_swgr.pdf}
	\caption{Trajectories of beam orbit before and after optimization using combination of particle swarm and gradient descent methods}
	\label{fig3}      
\end{figure}
\begin{figure}[t]
	\includegraphics[scale=0.58]{chart.pdf}
	\caption{Correctors horizontal angles before and after optimization}
	\label{fig4}      
\end{figure}



\section{Conclusion}

Results of modeling given above show that optimization of corrector elements in synchrotrons optic structure by using of the combination of stochastic determined optimization methods allows to significant improve beam orbit deviation in the case of presence tolerances of magnetic field.



%
\begin{acknowledgement}
The authors wish to thank O.S. Kozlov, V.A. Mikhaylov, D.A. Ovsyannikov, A.O. Sidorin, A.V. Tuzikov, G.V. Trubnikov from  Joint Institute for Nuclear Research for useful discussions on problems related to NICA Accelerator Complex and attention to the work done. This work was supported by Grants Council of the President of the Russian Federation.
\end{acknowledgement}
%


\bibliography{bibfile}
\end{document}
