function itegration

tend=100;


%%%%%%%%%%%%%%%%
for k=1:6
dt(k)=1/((2^(k+1)));
[T X] = euler(@Oscillator, [1; 0], dt(k), 0, tend, 10*(2^k));
xconv(1,k)=X(1,end);

[T X] = leapfrog(@Oscillator, [1; 0], dt(k), 0, tend, 10*(2^k));
T(end)
xconv(2,k)=X(1,end);

end

close all
figure;
plot(dt,xconv(2,:))

% figure;
% plot(dt,xconv(1,:))

figure;
plot(T,X)
%%%%%%%%%%%%%%%%

tend=4;

for k=1:6
dt(k)=4/((2^(k)));
[T X] = euler(@Dalkvist, [1], dt(k), 0, tend, 10*(2^k));
xconv(1,k)=X(1,end);
end

close all
figure;
plot(dt,xconv(1,:))

% figure;
% plot(dt,xconv(1,:))

figure;
plot(T,X)




fract = 3;
for k=1:6
% dt=0.2/(2*k);
x0=[1; 0];
T = 2000;
dt= 0.05;
ndt = T/dt;

% ndt = 50*fract^(k-1);
% dt= T/ndt;
% dtAr(k) = dt;


t=0;

x1=x0;
xAr1=x0;

x2=x0;
xAr2=x0;

x3=x0;
xAr3=x0;

x4=x0;
xAr4=x0;

x5=x0;
xAr5{k}=x0;

tAr=0;
for i=1:ndt
    x1=x1+dt*fdx(x1);
    
%     ff = fdx(x5);
%     x5(2,end)=x5(2,end)+dt*ff(2,end);
%     ff = fdx(x5);
%     x5(1,end)=x5(1,end)+dt*ff(1,end);


%     
    if(i==1)
         x2(:,2)=x2(:,1)+dt*fdx(x2(:,1));
    else
         x2(:,3)=x2(:,2)+0.5*dt*(fdx(x2(:,2))*3-fdx(x2(:,1)));

         x3(:,1) = x2(:,3);
         for k=1:5
            x3(:,1)=x2(:,2)+0.5*dt*(fdx(x2(:,2))+fdx(x3(:,1)));
         end
         
         x2(:,1)=x2(:,2);
         x2(:,2)=x2(:,3);
         
         
%             r=0.25*(x3(:,1)-x2(:,3));
%             x3(:,1)= x3(:,1)+r;
% %          r=x3(:,1)-x2(:,3)
%          x2(:,1) = x2(:,2); 
%          x2(:,2) = x2(:,3); 
%          
%          x4(:,3) = x4(:,1)+dt*fdx(x4(:,2));
%          x4(:,1) = x4(:,2);
%          x4(:,2) = x4(:,3);
    end
    t=t + dt;
% 
    if(mod(i,10)==0)
        xAr1 = [xAr1 x1(:,1)];
        xAr2 = [xAr2 x2(:,1)];
        xAr3 = [xAr3 x3(:,1)];
%         xAr4 = [xAr4 x4(:,1)];
%         xAr5 = [xAr5 x5(:,1)];
        tAr =[tAr t];
    end
%     if(mod(i,5)==0)
%         xAr5{k} = [xAr5{k} x5(:,1)];
%         tAr{k} =[tAr{k} t];
%     end
end


figure;
plot(tAr, xAr2(1,:),tAr, xAr3(1,:));

dt = T - t;

ff = fdx(x5);
x5(1,end)=x5(1,end)+dt*ff(1,end);
ff = fdx(x5);
x5(2,end)=x5(2,end)+dt*ff(2,end);

xEstAr(k) = x5(1,end);
% xEstAr(k) = x1(1,end);

end
figure;
% plot(dtAr, xEstAr);

% for k=2:length(xEstAr)-1
%     convRate(k-1)= log(abs((xEstAr(k-1)-xEstAr(k))/(xEstAr(k)-xEstAr(k+1))))/log(fract);
% end
figure;
plot(1:length(convRate), convRate);
figure;
plot(dtAr, xEstAr);

plot(tAr{1},xAr5{1}(1,:),tAr{2},xAr5{2}(1,:),tAr{end},xAr5{end}(1,:));
% ,tAr,xAr2(1,:),tAr,xAr3(1,:)
end

function [T X] = euler(f, x0, dt, t0, tend, nout)

X=[];
T=[];
Xcur = x0;
tcur=t0;
i=0;
while (tcur+dt<=tend)
    
   if(mod(i,nout)==0)
        X  = [X Xcur];
        T  = [T tcur];
   end
   Xcur=Xcur+dt*feval(f,Xcur);
   tcur = tcur+ dt;
end

dt = tend - tcur;
Xcur=Xcur+dt*feval(f,Xcur);
tcur = tcur + dt;

X  = [X Xcur];
T  = [T tcur];

end

function [T X] = leapfrog(f, x0, dt, t0, tend, nout)

X=[];
T=[];
Xcur = x0;
tcur=t0;
i=0;
while (tcur+dt<=tend)
    
   if(mod(i,nout)==0)
        X  = [X Xcur];
        T  = [T tcur];
   end
   tmp=feval(f,Xcur);
   Xcur(1,1)=Xcur(1,1)+dt*tmp(1,1);
   tmp=feval(f,Xcur);
   Xcur(2,1)=Xcur(2,1)+dt*tmp(2,1);

   tcur = tcur+ dt;
end

dt = tend - tcur;
tmp=feval(f,Xcur);
Xcur(1,1)=Xcur(1,1)+dt*tmp(1,1);

xtmp = Xcur;
% tmp=feval(f,Xcur);
% xtmp(2,1)=xtmp(2,1)+0.5*dt*tmp(2,1);
tmp=feval(f,xtmp);


Xcur(2,1)=Xcur(2,1)+dt*tmp(2,1);
tcur = tcur + dt;

X  = [X Xcur];
T  = [T tcur];

end

function dx=Dalkvist(x)
 dx(1,1)=-x(1,1);
end

function dx=Oscillator(x)
dx(1,1)=x(2,1);
dx(2,1)=-x(1,1)+cos(x(1,1))^2;
end

function dx=Oscillator1(x)
dx(1,1)=x(2,1);
dx(2,1)=-x(1,1)-0.2*(x(2,1));
end