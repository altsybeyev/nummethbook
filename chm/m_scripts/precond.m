function t

i=0;

for n=4:5:340

    i=i+1;

    A=hilb(n);
    b=rand(n, 1);

    C=zeros(n,n);
    
    condAr(i)=cond(A);
    
   
    [L,U] = ilu(sparse(A),struct('type','ilutp','droptol',1e-3));
    M0=inv(U)*inv(L);
    
    AA=M0*A;
    bb=M0*b;
  
    x1=linsolve(A,b);
    x2=linsolve(AA,bb);

    cond(A)  
    cond(AA)

    r1Ar(i)=slau_sol_norm(A,b,x1);
    r2Ar(i)=slau_sol_norm(AA,bb,x2);
    r3Ar(i)=slau_sol_norm(A,b,x2);
end
[condAr, IX]=sort(condAr);
r1Ar = r1Ar(IX);
r2Ar = r2Ar(IX);
r3Ar = r3Ar(IX);

figure;
loglog(condAr, r1Ar,condAr, r1Ar,'kd',...
       condAr, r2Ar,condAr, r2Ar,'kd',...
       condAr, r3Ar,condAr, r3Ar,'kd');

end