function [x, iter_tol] = SOR(A, b, omega, tol, max_iter)
cur_iter = 0;
iter_tol = [];
cur_tol = inf;
x=ones(length(b),1);
while(cur_iter < max_iter && cur_tol>tol)
    cur_iter = cur_iter+1;
    omega_sor = omega;
    if(cur_iter == 1)
        omega_sor = 1;
    end
    x=SOR_iteration(x,A,b,omega_sor);
    iter_tol=[iter_tol max(max(abs((A*x-b)/b)))];
    cur_tol = iter_tol(end);
end
end