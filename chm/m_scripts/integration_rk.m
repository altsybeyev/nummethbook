function  integration_rk

tend=6000;
lambda = 1;

for k=1:5
    dt(k)=tend/(10*(4^(k+1)));
%     [T1, X1] = leapfrog( @(x) oscillator(x), [1; 0], dt(k), 0, tend, 1);
    [T1, X1] = ode45( @(t,x) oscillator(x), [0 tend] ,[1; 0]);

    r(1,k)=abs(X1(1,end) - cos(tend));
end

% figure;
% plot(dt,r)

figure;
dtgrid = 0:dt(end):tend;
% plot(T1,X1(1,:),dtgrid,cos(dtgrid));
plot(T1,X1(:,1),dtgrid,cos(dtgrid));

close all

end

