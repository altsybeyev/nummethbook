function result = slau_sol_norm(A,b,x)
rr=b-A*x;
result = norm(rr)./norm(b);
end

