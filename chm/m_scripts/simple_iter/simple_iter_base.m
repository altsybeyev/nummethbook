function simple_iter_base( x0, epsConv, get_new_x  )

x(1) = x0;

rate=[];
i=0;

while (i<2000)
    
    new_x = get_new_x(x(end));
       
    x=[x; new_x];
       
    if estimate_convergence(x(end), x(end-1), epsConv )
        break
    end
    
    if(i>3)
        rate=[rate get_convergence_rate(x(end),x(end-1),x(end-2),x(end-3))];
    end
    i=i+1;
end

figure;
plot(1:length(x),x);
% figure;
% plot(1:length(rate),rate);

end
