n=5000;
b=rand(n,1);
% A = rand(n);
% A=A*A.';
diagm = rand(1,n-1);
A = diag(2+rand(1,n)) + diag(diagm,1) + diag(diagm,-1);

tic
try chol(A);
    disp('Matrix is symmetric positive definite.')
catch ME
    disp('Matrix is not symmetric positive definite')
    return
end
toc

iter_tol_arr = {};

figure;

for i=0:4
    omega=1+0.1*i;
    tic
    [x_r, iter_tol] = SOR(A, b, omega, 1e-5, 10000);
    toc
    length(iter_tol)
    iter_tol_arr{i+1} = iter_tol;
    hold on;
    plot(iter_tol);
end