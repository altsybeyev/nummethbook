function t

n=2000;

A=sprand(n,n,0.1);
b=rand(size(A,1), 1);

tic
[L,U] = ilu(sparse(A),struct('type','ilutp','droptol',1e-5));
[x2,fl2,rr2,it2,rv2]=bicgstab(A,b,1e-8,50,L,U);
toc

figure;
semilogy(1:length(rv2),rv2/norm(b),'-o');
tic
[x3,fl3,rr3,it3,rv3]=gmres(A,b,10,1e-8,50,L,U);
toc
figure;
semilogy(1:length(rv3),rv3/norm(b),'-o');
end