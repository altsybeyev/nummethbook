function [ output_args ] = testX( input_args )
%TESTX=F(X) Summary of this function goes here
%   Detailed explanation goes here

x0=0;
x(1) = 1.7;

ni=1000;

eps = 0.02;

epsConv =0.00001;

k=1*epsConv;
omega = [];
i=1;
while (1)
    
    f = Fx(x(end),eps);
    
    omega = [omega; k*x(end)/(eps*f+k*x(end)-k*f)];
    
    omega = [omega; 0.3];
    
    F = (1-omega(i))*x(end)+omega(i)*f;
    x=[x; F];
    if(abs(x(end)-x(end-1))/x(end)<epsConv)
        break
    end

    i=i+1;
end

figure;
plot(1:i+1,x);

figure;
plot(1:i,omega(1:end));
end

function F = Fx(x, eps)
F = (x-1)^2+1;
F = F + unifrnd(-eps*F,eps*F);
end