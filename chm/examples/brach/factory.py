from abc import ABCMeta, abstractmethod

import models
import methods

class IModelFactory(metaclass=ABCMeta):
    def __init__(self, geometry):
        self._geometry = geometry

    @abstractmethod
    def make_geometry_model(self, l) -> models.IGeometryModel:
        pass

    @abstractmethod
    def make_coordinates(self) -> models.ICoordinates:
        pass

    def make_positiones_updater(self, position_updater_type) -> methods.IPositionUpdater:
        return methods.dict_updaters[position_updater_type]


class FactorySt(IModelFactory):

    def __init__(self, geometry):
        IModelFactory.__init__(self, geometry)

    def make_coordinates(self) -> models.ICoordinates:
        return models.CoordinatesSt(self._geometry)

    def make_geometry_model(self, l) -> models.IGeometryModel:
        return models.GeometryModelSt(self._geometry, l)

dict_factories = {}