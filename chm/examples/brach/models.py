from abc import ABCMeta, abstractmethod
import math
import bisect


class Geometry:
    def __init__(self, params):
        self.__L = params['L']
        self.__H = params['H']

    def get_L(self):
        return self.__L

    def get_H(self):
        return self.__H

    def get_vmax(self):
        return math.sqrt(2*9.81*self.__H)


class ICoordinates(metaclass=ABCMeta):
    def __init__(self, geometry):
        self._geometry = geometry

    @abstractmethod
    def get_right_side(self, angle):
        pass


class IGeometryModel(metaclass=ABCMeta):
    def __init__(self, geometry, l):
        self._geometry = geometry

    @abstractmethod
    def get_angle_l(self, coordinates):
        pass

    @abstractmethod
    def estimate_timestep(self):
        pass

    @abstractmethod
    def distance(self, coordinates):
        pass

    @abstractmethod
    def get_x_arr(self):
        pass

    @abstractmethod
    def get_y_arr(self):
        pass


class CoordinatesSt(ICoordinates):
    def __init__(self, geometry):
        ICoordinates.__init__(self, geometry)
        self.__s = 0
        self.__vs = 0

    def get_s(self):
        return self.__s

    def get_vs(self):
        return self.__vs

    def get_right_side(self, angle):
        return [9.81*math.sin(angle), self.__vs]

    def update_positions(self, new_s, new_vs):
        self.__s = new_s
        self.__vs = new_vs


class GeometryModelSt(IGeometryModel):
    def append_data(self, l_cur, alpha, l, x, y):
        self.__ls.append(l)
        self.__alphas.append(alpha)
        self.__s.append(l_cur)
        self.__y.append(y)
        self.__x.append(x)

    def __init__(self, geometry, l):  # make default curve as simple line
        IGeometryModel.__init__(self, geometry, l)
        alpha = math.atan(self._geometry.get_H() / self._geometry.get_L())
        self.__ls = []
        self.__alphas = []
        self.__s = []
        self.__y = []
        self.__x = []

        new_x = 0
        new_y = self._geometry.get_H()
        l_cur = 0
        while new_x < self._geometry.get_L():
            self.append_data(l_cur, alpha, l, new_x, new_y)
            new_x = new_x + l*math.cos(alpha)
            new_y = new_y - l*math.sin(alpha)
            l_cur = l_cur + l

        l_last = l - \
            math.sqrt(math.pow(new_x - self._geometry.get_L(), 2) +
                      math.pow(new_y, 2))
        new_x = new_x - l*math.cos(alpha) + l_last*math.cos(alpha)
        new_y = new_y + l*math.sin(alpha) - l_last*math.sin(alpha)
        self.append_data(l_cur + l_last - l, alpha, l_last, new_x, new_y)

    def change_y(self, new_y):

        for i in range(1, len(self.__y)):
            delta_x_ = self.__x[i] - self.__x[i - 1]

            if i != len(self.__y) - 1:
                delta_y_ = self.__y[i - 1] - new_y[i]
            else:
                delta_y_ = new_y[-2] - self.__y[-1]
                
            new_l_ = math.sqrt(math.pow(delta_x_, 2) +
                               math.pow(delta_y_, 2))

            new_alpha_ = math.asin(delta_y_ / new_l_)

            self.__y[i] = new_y[i]
            self.__alphas[i] = new_alpha_
            self.__ls[i] = new_l_
            self.__s[i] = self.__s[i - 1] + new_l_

        # delta_x_ = self.__x[-1] - self.__x[-2]
        # delta_y_ = new_y[-2] - self.__y[-1]
        # new_l_ = math.sqrt(math.pow(delta_x_, 2) +
        #                 math.pow(delta_y_, 2))

    def get_angle_l(self, coordinates):
        ind = bisect.bisect(self.__s, coordinates.get_s())
        return self.__alphas[ind - 1], self.__ls[ind - 1]

    def estimate_timestep(self):
        return self.__ls[0] / self._geometry.get_vmax()

    def distance(self, coordinates):
        return coordinates.get_s() - self.__s[-1]

    def get_x_arr(self):
        return self.__x

    def get_y_arr(self):
        return self.__y
