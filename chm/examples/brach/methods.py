from abc import ABCMeta, abstractmethod
import math

class IPositionUpdater(metaclass=ABCMeta):
    @abstractmethod
    def update(self, timestep, geometry_model, coordinates_model):
        pass


class PositionUpdaterSimple(IPositionUpdater):
    def update(self, timestep, geometry_model, coordinates_model):
        angle, l = geometry_model.get_angle_l(coordinates_model)
        right_side = coordinates_model.get_right_side(angle)
        new_v = coordinates_model.get_vs() + right_side[0]*timestep
        coordinates_model.update_positions(coordinates_model.get_s(), new_v)

        angle, l = geometry_model.get_angle_l(coordinates_model)
        right_side = coordinates_model.get_right_side(angle)
        new_s = coordinates_model.get_s() + right_side[1]*timestep
        coordinates_model.update_positions(new_s, coordinates_model.get_vs())


dict_updaters = {}
dict_updaters['simple'] = PositionUpdaterSimple()