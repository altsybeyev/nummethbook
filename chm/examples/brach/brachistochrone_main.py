import sys
import json

import factory
import models
import copy
import math
import matplotlib.pyplot as plt
import numpy

from scipy.optimize import minimize
from pyswarm import pso
from scipy import interpolate


def calculate(geometry_model, coordinates_model, positiones_updater):

    def do_step(step):
        positiones_updater.update(step, geometry_model, coordinates_model)

    time_step = geometry_model.estimate_timestep() / 50

    time = 0

    dist1 = -1

    i = 0

    while dist1 < 0 and i < 1e4:
        do_step(time_step)
        time = time + time_step
        dist1 = geometry_model.distance(coordinates_model)
        i = i+1

    if i > 1e4 - 1:
        tt = 0
        return  coordinates_model.get_vs(), time+ 1e9
        # plt.plot(geometry_model.get_y_arr())
        # plt.show()

    # more accurate do the last step
    do_step(-time_step)
    time = time - time_step
    dist2 = geometry_model.distance(coordinates_model)
    last_timestep = time_step*abs(dist2)/(abs(dist2)+dist1)
    do_step(last_timestep)

    return coordinates_model.get_vs(), time


def run_using_data_dict_and_geam(dict, geometry_model):

    # print('analytical vmax = ' + str(geometry.get_vmax()))

    model_factory = factory.dict_factories[dict['model']['type']]

    positiones_updater = model_factory.make_positiones_updater(
        dict['model']['position_updater'])

    coordinates_model = model_factory.make_coordinates()

    vmax, t = calculate(geometry_model, coordinates_model, positiones_updater)

    return t


def approx2d(L, H, c1, x):
    c2 = (-H-c1*L*L) / L
    return x*x*c1 + x*c2 + H


def approx3d(L, H, c1, c2, x):
    c3 = (-H-c2*L*L-c1*L*L*L) / L
    return x*x*x*c1 + x*x*c2 + x*c3 + H

def approx4d(L, H, c1, c2, c3, x):
    c4 = (-H-c3*L*L-c2*L*L*L-c1*L*L*L*L) / L
    return x*x*x*x*c1 +x*x*x*c2 + x*x*c3 + x*c4 + H

def get_an_sol(geometry, size):

    x_att = numpy.linspace(1e-3, 2*math.pi - 1e-3, 1e6)

    delta = sol(x_att, geometry.get_L(), geometry.get_H())
    delta = delta * delta

    i_min = numpy.where(delta == numpy.amin(delta))

    t = x_att[i_min]
    c = geometry.get_H()/(1-numpy.cos(t))

    t_arr = numpy.linspace(0, t[0], num=size)
    x_arr = c*(t_arr - numpy.sin(t_arr))
    y_arr = -c*(1 - numpy.cos(t_arr)) + 1

    x_arr[-1] = geometry.get_L()

    x_arr_ = numpy.linspace(0, geometry.get_L(), size)


    f = interpolate.interp1d(x_arr, y_arr)

    y_arr_x = f(x_arr_)

    return x_arr_, y_arr_x



def run_using_data_dict(dict):
    geometry = models.Geometry(dict['main_parameters'])

    v_an = geometry.get_vmax()

    factory.dict_factories['st'] = factory.FactorySt(geometry)

    model_factory = factory.dict_factories[dict['model']['type']]
    geometry_model = model_factory.make_geometry_model(dict['model']['l'])
    y_start = numpy.copy(geometry_model.get_y_arr())
    y_start_opt = numpy.copy(geometry_model.get_y_arr()[1:-1])

    x_values = numpy.copy(geometry_model.get_x_arr()[1:-1])

    # for i in range(0, len(y_an)):
    #     y_start[i] = y_an[i]

    res1 = run_using_data_dict_and_geam(dict, geometry_model)
    print('strait line time = ' + str(res1))

    x_an, y_an = get_an_sol(geometry, len(y_start))

    geometry_model.change_y(y_an)

    res2 = run_using_data_dict_and_geam(dict, geometry_model)
    print('analitical time = ' + str(res2))

    # plt.plot( x_an, y_an)
    # plt.show()

    def fitnes_naive(y):
        new_y = y_start
        new_y[1:-1] = y
        geometry_model.change_y(new_y)
        for i in range(1, len(y)):
            if y[i] > y[0]:
                return 1e9
        return run_using_data_dict_and_geam(dict, geometry_model)

    start_fit = fitnes_naive(y_start_opt)

    # for i in range(0, 5):
    #     print(start_fit)
    #     delta = y_start_opt[0] * 0.01
    #     grad = numpy.zeros(len(y_start_opt))

    #     for i in range(0, len(y_start_opt)):
    #         y_new = numpy.copy(y_start_opt)
    #         delta_cur = delta

    #         while grad[i] == 0:
    #             y_new[i] = y_start_opt[i] + delta_cur
    #             t = fitnes_naive(y_new)
    #             grad[i] = (t - start_fit) / delta_cur
    #             delta_cur = delta_cur*2.1

    #     start_fit_old = start_fit
    #     h_start = 0.05

    #     while start_fit >= start_fit_old and h_start > 1e-16:
    #         y_curr = numpy.copy(y_start_opt)
    #         y_curr = y_curr - grad*h_start
    #         start_fit = fitnes_naive(y_curr)
    #         h_start = h_start*0.5

    #     y_start_opt = y_curr

    # print(fitnes(y_start_opt))
    # lb = numpy.array(y_an[1:-2])

    # ub = numpy.array(y_start_opt)
    # lb = numpy.array(y_start_opt)

    # for i in range(1, len(y_an) - 1):
    #     lb[i-1] = y_an[i]

    # xopt, fopt = pso(fitnes, lb, ub, maxiter=500, debug=True)

    # def fitnes_2d_approx(c):
    #     new_y = approx2d(geometry.get_L(), geometry.get_H(), c, x_values)
    #     return fitnes_naive(new_y)

    # res = minimize(fitnes_2d_approx, 1, method='nelder-mead',
    #                options={'xtol': 1e-9, 'disp': True})

    # new_y = approx2d(geometry.get_L(), geometry.get_H(), res.x, x_values)

    def fitnes_4d_approx(c):
        new_y = approx4d(geometry.get_L(), geometry.get_H(),
                         c[0], c[1], c[2], x_values)
        return fitnes_naive(new_y)

    res = minimize(fitnes_4d_approx, [0.01, 0.2, 1], method='nelder-mead',
                   options={'xtol': 1e-9, 'disp': True})

    print(res.x)

    new_y = approx4d(geometry.get_L(), geometry.get_H(),
                     res.x[0], res.x[1],res.x[2], x_values)



    plt.plot(x_values, new_y, x_an, y_an)
    plt.show()

    return x_values, new_y
    # plt.plot(x_values, new_y)
    # plt.show()

    # y_start[1]=y_start[1]*0.99
    # y_start[2]=y_start[2]*0.99

    # print(fitnes(y_start))


def est_conv(dict, n_calc):
    l_start = dict['model']['l']

    delta_arr = []
    rates_1 = []
    rates_2 = []

    l_arr = []
    v_num_arr = []

    for i in range(0, n_calc):
        l_current = l_start / pow(2, i)
        l_arr.append(l_current)
        dict['model']['l'] = l_current
        v_an, v_num, t = run_using_data_dict(dict)
        v_num_arr.append(v_num)
        delta_arr.append(abs((v_num-v_an)))
        if i > 1:
            rates_1.append(
                math.log2(abs((v_num_arr[-2] - v_an)/(v_num_arr[-1] - v_an))))
        if i > 2:
            rates_2.append(math.log2(
                abs((v_num_arr[-3] - v_num_arr[-2])/(v_num_arr[-2] - v_num_arr[-1]))))

    fig, axs = plt.subplots(2, 1, sharex=True)

    axs[0].plot(l_arr, delta_arr)
    # axs[0].plot(l_arr, l_arr)

    axs[1].plot(l_arr[:-3], rates_2)

    # axs[0].set_yscale('log')
    # axs[0].set_xscale('log')
    # axs[1].set_xscale('log')

    plt.show()


##########
data = {}
if len(sys.argv) != 2:
    print('incorrect number of arguments')
    sys.exit()

try:
    with open(str(sys.argv[1])) as json_file:
        data = json.load(json_file)
except Exception as err:
    print('Error read config: ' + str(err))
    sys.exit()

n_calc = 8


# est_conv(data, n_calc)

def sol(x, L, H):
    return (x-numpy.sin(x)) / (1-numpy.cos(x)) - L/H



run_using_data_dict(data)
